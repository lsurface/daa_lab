#include <iostream>

using namespace std;

int main() {

    int r, c;

    cout << "Enter number of rows\n";
    cin >> r;

    cout << "Enter numer of columns\n";
    cin >> c;

    int A[r][c], row_sum[r] = {0}, column_sum[c] = {0};

    // Entering matrix elements
    for (int i = 0; i < r; i++) {
        for (int j = 0; j < c; j++) {
            cout << "Enter value for A[" << i << "][" << j << "]\n";
            cin >> A[i][j];
        }
    }

    // Calculating row sums
    for (int i = 0; i < r; i++) {
        for (int j = 0; j < c; j++) {
            row_sum[i] += A[i][j];
        }
    }

    // Calculating column_sum
    for (int j = 0; j < c; j++) {
        for (int i = 0; i < r; i++) {
            column_sum[j] += A[i][j];
        }
    }

    // Display row sums
    for (int i = 0; i < r; i++) {
        cout << "Sum of row " << i + 1 << " is " << row_sum[i] << "\n";
    }

    // Display column sums
    for (int i = 0; i < c; i++) {
        cout << "Sum of column " << i + 1 << " is " << column_sum[i] << "\n";
    }

    return 0;
}