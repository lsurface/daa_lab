/*
 * Prim's Algorithm
 */
#include <cstring>
#include <iostream>
using namespace std;

#define INF 9999999

int main() {
    int no_edge = 0;
    int V;

    cout << "Enter number of vertices: ";
    cin >> V;

    int Graph[V][V];

    for (int i = 0; i < V; i++) {
        for (int j = 0; j < V; j++) {
            cout << "Enter weight for G[" << i << "][" << j << "]: ";
            cin >> Graph[i][j];
        }
    }

    int selected[V];

    // set selected false initially using in-built function
    memset(selected, false, sizeof(selected));

    // set number of edge to 0
    no_edge = 0;

    selected[0] = true;

    int x;  //  row number
    int y;  //  col number

    // print for edge and weight
    cout << "Edge"
         << " : "
         << "Weight"
         << "\n";

    while (no_edge < V - 1) {

        int min = INF;
        x = 0;
        y = 0;

        for (int i = 0; i < V; i++) {
            if (selected[i]) {
                for (int j = 0; j < V; j++) {
                    if (!selected[j] && Graph[i][j]) {
                        if (min > Graph[i][j]) {
                            min = Graph[i][j];
                            x = i;
                            y = j;
                        }
                    }
                }
            }
        }
        cout << x << " - " << y << " :  " << Graph[x][y] << "\n";

        selected[y] = true;
        no_edge++;
    }

    return 0;
}