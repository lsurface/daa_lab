#include <iostream>

using namespace std;

int F(int);
int F_itr(int);

int main() {

    int n, Fn;

    cout << "Enter value for n\n";
    cin >> n;

    // Finding Fibonacci number using recursive function
    Fn = F(n);
    cout << "Answer through recursive is " << Fn << "\n";

    // Finding Fibonacci number using iterative function
    Fn = F_itr(n);
    cout << "Answer through iterative is " << Fn << "\n";

    return 0;
}

int F(int n) {

    if (n <= 1) {
        return n;
    } else {
        return F(n - 1) + F(n - 2);
    }
}

int F_itr(int n) {

    int F_n_1 = 0;
    int F_n_2 = 1;
    int F;

    for (int i = 1; i <= n; i++) {
        F = F_n_1 + F_n_2;
        F_n_2 = F_n_1;
        F_n_1 = F;
    }
    return F;
}