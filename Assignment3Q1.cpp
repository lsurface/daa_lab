#include <iostream>

using namespace std;

// Merge two subarrays L and M into arr
void merge(int arr[], int p, int q, int r) {

    // Create left and right sub arrays
    int n1 = q - p + 1;
    int n2 = r - q;

    int L[n1], R[n2];

    for (int i = 0; i < n1; i++) {
        L[i] = arr[p + i];
    }

    for (int j = 0; j < n2; j++) {
        R[j] = arr[q + 1 + j];
    }

    // Maintain current index of sub-arrays and main array
    int i, j, k;
    i = 0;
    j = 0;
    k = p;

    while (i < n1 && j < n2) {
        if (L[i] <= R[j]) {
            arr[k] = L[i];
            i++;
        } else {
            arr[k] = R[j];
            j++;
        }

        k++;
    }

    // Inserting elements of left sub array if any left
    while (i < n1) {
        arr[k] = L[i];
        i++;
        k++;
    }

    // Inserting elements of right sub array if any left
    while (j < n2) {
        arr[k] = R[j];
        j++;
        k++;
    }
}

// Divide the array into two subarrays, sort them and merge them
void mergeSort(int arr[], int l, int r) {

    if (l < r) {

        int m = l + (r - l) / 2;

        mergeSort(arr, l, m);
        mergeSort(arr, m + 1, r);

        // Merge the sorted subarrays
        merge(arr, l, m, r);
    }
}

// Print the array
void printArray(int arr[], int size) {
    for (int i = 0; i < size; i++) cout << arr[i] << " ";
    cout << "\n";
}

int main() {
    int n;

    cout << "Enter size of array: ";
    cin >> n;

    int arr[n];

    for (int i = 0; i < n; i++) {
        cout << "Enter value for a[" << i << "]: ";
        cin >> arr[i];
    }

    cout << "Unsorted array:\t";
    printArray(arr, n);

    mergeSort(arr, 0, n - 1);

    cout << "Sorted array:\t";
    printArray(arr, n);

    return 0;
}