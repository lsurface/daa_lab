#include <iostream>

using namespace std;

// Swapping the array elements
void swap(int *a, int *b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}

// Bubblesort algorithm
void bubbleSort(int arr[], int n) {

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n - i; j++) {
            if (arr[j] > arr[j + 1]) {
                swap(&arr[j], &arr[j + 1]);
            }
        }
    }
}

// Displaying the array
void print_array(int arr[], int n) {

    for (int i = 0; i < n; i++) {
        cout << arr[i] << " ";
    }
    cout << "\n";
}

int main() {

    int n;

    cout << "Enter array size: ";
    cin >> n;

    int arr[n];

    for (int i = 0; i < n; i++) {
        cout << "Enter value for a[" << i << "]: ";
        cin >> arr[i];
    }

    cout << "\nUnsorted array:\t";
    print_array(arr, n);

    bubbleSort(arr, n);

    cout << "Sorted array:\t";
    print_array(arr, n);

    return 0;
}