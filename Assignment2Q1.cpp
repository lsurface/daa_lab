#include <iostream>

using namespace std;

int main() {

    int n;

    cout << "Enter number of elements\n";
    cin >> n;

    int arr[n];

    cout << "Enter array values\n";

    for (int i = 0; i < n; i++) {
        cin >> arr[i];
    }

    // Calculating sum and product of elements on even and odd index
    int sum_even = 0, sum_odd = 0;
    int product_even = 1, product_odd = 1;

    for (int i = 0; i < n; i++) {

        if (i % 2 == 0) {

            sum_odd += arr[i];
            product_odd *= arr[i];

        } else {
            sum_even += arr[i];
            product_even *= arr[i];
        }
    }

    cout << "Sum of even position elements is " << sum_even << "\n";
    cout << "Sum of odd position elements is " << sum_odd << "\n";

    cout << "Product of even position elements is " << product_even << "\n";
    cout << "Product of odd position elements is " << product_odd << "\n";

    return 0;
}