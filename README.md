# README #



### What is this repository for? ###

This repository is for assignments covered in practicals for Design and Analysis of Algorithms

1. Assignment 1: Sum of rows and columns of a matrix
2. Assignment 2: Finding the sum and product of array elements on even and odd positions and Fibonacci number
3. Assignment 3: Mergesort and Bubblesort
4. Assignment 4: Kruskal's Algorithm and Prim's Algorithm