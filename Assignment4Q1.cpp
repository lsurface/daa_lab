/*
 * Kruskal's Algorithm
 */
#include <iostream>

using namespace std;

class Set {
 public:
    int parent;
    int rank;
};

class Edge {
 public:
    int vertex_1;
    int vertex_2;
    int weight;
};

class Graph {
 public:
    int V;  // Number of vertices
    int E;  // Number of edges

    Edge *edge;
};

// Creating graph
Graph *create_Graph(int V, int E) {
    Graph *graph = new Graph();

    graph->V = V;
    graph->E = E;
    graph->edge = new Edge[E];

    return graph;
}

int find(Set set_array[], int x) {
    if (set_array[x].parent != x) {
        set_array[x].parent = find(set_array, set_array[x].parent);
    }

    return set_array[x].parent;
}
void Union(Set set_array[], int x, int y) {
    int xroot = find(set_array, x);
    int yroot = find(set_array, y);

    if (set_array[xroot].rank < set_array[yroot].rank) {
        set_array[xroot].parent = yroot;
    } else if (set_array[xroot].rank > set_array[yroot].rank)
        set_array[yroot].parent = xroot;
    else {
        set_array[yroot].parent = xroot;
        set_array[xroot].rank++;
    }
}

int myComp(const void *a, const void *b) {
    Edge *a1 = (Edge *)a;
    Edge *b1 = (Edge *)b;
    return a1->weight > b1->weight;
}

void MST_Kruskal(Graph *graph) {

    int V = graph->V;
    Edge result[V];
    qsort(graph->edge, graph->E, sizeof(graph->edge[0]), myComp);
    Set *set_array = new Set[(V * sizeof(Set))];

    for (int i = 0; i < V; i++) {
        set_array[i].parent = i;
        set_array[i].rank = 0;
    }

    int e = 0;  // Till number of edges in MST becomes V-1
    int k = 0;

    while (e < V - 1 && k < graph->E) {
        Edge next_edge = graph->edge[k++];

        int x = find(set_array, next_edge.vertex_1);
        int y = find(set_array, next_edge.vertex_2);

        if (x != y) {
            result[e++] = next_edge;
            Union(set_array, x, y);
        }
    }

    cout << "Following are the edges in the constructed MST\n";
    for (int i = 0; i < e; ++i) {
        cout << result[i].vertex_1 << " -- " << result[i].vertex_2
             << " == " << result[i].weight << "\n";
    }
}

int main() {

    int V, E;

    cout << "Enter number of vertices: ";
    cin >> V;

    cout << "Enter number of edges: ";
    cin >> E;

    // Creating graph
    Graph *graph = create_Graph(V, E);

    // Defining edges
    for (int i = 0; i < E; i++) {

        cout << "Enter source: ";
        cin >> graph->edge[i].vertex_1;

        cout << "Enter destination: ";
        cin >> graph->edge[i].vertex_2;

        cout << "Enter weight: ";
        cin >> graph->edge[i].weight;
    }

    MST_Kruskal(graph);

    return 0;
}